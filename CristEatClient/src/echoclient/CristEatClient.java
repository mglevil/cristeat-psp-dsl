/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package echoclient;

import Models.Restaurant;
import Models.Plato;
import Models.Client;
import Models.Propietario;
import Views.View_client;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Base64;

/**
 *
 * @author mglevil
 */
public class CristEatClient{
    private String hostName;
    private int portNumber;
    private String credenciales;
    private Client cl;
    private int total_rest;
    private static ArrayList<Restaurant> rests;
    private static Propietario propietario;
    public static ArrayList<Plato> platos;
    public static boolean isPropietario = true;
    public static String iAm = null;
    public static String havePlats = null;
    public static boolean badLogin = false;
    public static int idClient;
    private View_client vcl;
    private final int sizePack = 512;
    private FileOutputStream fosImg;
    private FileOutputStream fosVid;
    private byte[] buffer;   
    public static String idPlato;
    public static String pathImg;
    public static String pathVid;
    private File img;
    private File vid;
    public static int complete = 0;
    public static boolean dataPlato = false;
    public static String idNewPlat;
    
    static Socket echoSocket;
    private PrintWriter out;
    private BufferedReader in;
    static String login;
    
    public CristEatClient(){}
    public CristEatClient(String host, int port, String login, String passwd){    
        this.hostName = host;
        this.portNumber = port;
        this.vcl = new View_client(null);
        this.login = login;
        this.credenciales = "PROTOCOLCRISTEAT1.0#LOGIN#"+ login +"#" + passwd;      
        this.cl = null;
        this.rests = null;
        this.total_rest = 0;
        this.propietario = null;  
        Utils.cleanDir();
        listenServer();
    }
    
    public void postMessageToServer(String message){
        try {
            out = new PrintWriter(echoSocket.getOutputStream(), true);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        out.println("PROTOCOLCRISTEAT1.0#" + this.login + message);
    }   
           
    private void listenServer(){
        try { 
            echoSocket = new Socket(hostName, portNumber);
            out = new PrintWriter(echoSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
            
            String fromServer;
            String fromUser;
            
            fromUser = this.credenciales;
            out.println(fromUser);//enviar login
            
            while ((fromServer = in.readLine()) != null) {
                /*LOGIN*/
                if (fromServer.contains("BAD_LOGIN")){ 
                    badLogin = true;
                }else if(fromServer.contains("LOGIN_CORRECT") && fromServer.contains("CLIENTE")){
                    badLogin = false;
                    iAm = "c";
                    isPropietario = false;
                    cl = Utils.getClient(fromServer);
                    total_rest = Utils.getTotalRestaurantes(fromServer);
                    rests = Utils.getRestaurantes(fromServer);
                    idClient = cl.getId();          
                }else if(fromServer.contains("LOGIN_CORRECT") && fromServer.contains("PROPIETARIO")){
                    badLogin = false;
                    isPropietario = true;
                    propietario = Utils.getInfoPropietario(fromServer);
                    iAm = "p";
                }    
                /*Platos cliente*/
                if(fromServer.contains("#RESPONSE_GET_PLATOS#")){
                    platos = Utils.getPlatosForViewClient(fromServer);
                    havePlats = "ok";
                }
                
                /*BROADCAST*/
                if(fromServer.contains("#BROADCAST#CHANGE_STATE#") && !isPropietario){
                    
                    if(fromServer.contains("ONLINE")){
                        Restaurant rest = Utils.getRest(fromServer);
                        vcl.addRestBroadcast(rest);      
                    }else{
                        vcl.deleteRestBroadCast(fromServer.split("#")[5]);     
                    }
                }
                /*Cambio de estado confirmado*/
                if(fromServer.contains("CONFIRMED_STATE_CHANGE") && isPropietario){
                    //mostrar algo en la vista?
                }             
                
                /*FILE TRANSFER*/
                if(fromServer.contains("#STARTING_MULTIMEDIA_TRANSMISSION#")){
                    buffer = new byte[sizePack];
                    img = new File("data/details_platos/" + idPlato + ".png");
                    vid = new File("data/details_platos/" + idPlato + ".mp4");
                    pathImg = img.toString();
                    pathVid = vid.toString();
                    
                    if (!img.exists()) {
                        img.createNewFile();
                        this.fosImg = new FileOutputStream(img);                                         
                    }
                    
                    if(!vid.exists()){
                        vid.createNewFile();      
                        this.fosVid = new FileOutputStream(vid); 
                    }
                                    
                }
                
               if(fromServer.contains("#RESPONSE_MULTIMEDIA_PHOTO#")){
                    try {          
                        this.buffer = Base64.getDecoder().decode(Utils.getTransmissionBytes(fromServer).getBytes());
                        if(this.buffer != null)this.fosImg.write(this.buffer);                                  
                    } catch (IOException ex) {
                        System.err.println(ex.getMessage());
                    }
                    
                    
                }else if(fromServer.contains("#RESPONSE_MULTIMEDIA_VIDEO#")){                
                    try {                     
                        this.buffer = Base64.getDecoder().decode(Utils.getTransmissionBytes(fromServer).getBytes());
                        if(this.buffer != null)this.fosVid.write(buffer);                        
                    } catch (IOException ex) {
                        System.err.println(ex.getMessage());
                    }
                }
                                              
                if(fromServer.contains("#ENDING_MULTIMEDIA_TRANSMISSION#")){
                    complete += 1;
                    this.fosImg.close();
                    this.fosVid.close();
                }
                
                if(fromServer.contains("#RECEIVED_SUBIR_INFO_PLATO#")){
                    dataPlato = true;
                    idNewPlat = Utils.getIdNewPlat(fromServer);
                }
                
                //System.out.println(fromServer);
            }

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " + hostName);
            System.exit(1);
        }       
    }

    public boolean getIsPropietario() {
        return isPropietario;
    }

    public static Propietario getPropietario() {
        return propietario;
    }
    
    public Client getCl() {
        return cl;
    }

    public void setCl(Client cl) {
        this.cl = cl;
    }

    public int getTotal_rest() {
        return total_rest;
    }

    public void setTotal_rest(int total_rest) {
        this.total_rest = total_rest;
    }

    public static ArrayList<Restaurant> getRests() {
        return rests;
    }
        
}
