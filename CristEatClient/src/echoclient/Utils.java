/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package echoclient;

import Models.Restaurant;
import Models.Plato;
import Models.Client;
import Models.Propietario;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

/**
 *
 * @author mglevil
 */
public class Utils {
    
    public static void cleanDir(){
        File dir = new File("data/details_platos/");
        dir.mkdirs();
        File f;
        if (dir.isDirectory()) {
            String[] files = dir.list();
            if (files.length > 0) {
                for (String archivo : files) {
                    f = new File(dir + File.separator + archivo);

                    if (archivo.contains(".png") || archivo.contains(".mp4")) {
                        //System.out.println("Borrado:" + archivo);
                        f.delete();
                        f.deleteOnExit();
                    }
                }
            }
        }
    }
    
    //PROTOCOLCRISTEAT1.0#SERVER#LOGIN_CORRECT#[PROPIETARIO | CLIENTE]#[INFOPROPIA]
    public static Propietario getInfoPropietario(String cadena){
        Propietario propietario = null;
        Restaurant rest = null;
        Plato plat = null;
        
        ArrayList<Plato> platos = new ArrayList();
        //List<String> array = Arrays.asList(cadena.split("PROPIETARIO")[1].split("#"));
        String[] array = cadena.split("PROPIETARIO")[1].split("#");
        String[] allObject = new String[array.length];

        /*obtener platos*/
        allObject = Arrays.copyOfRange(array, 12, array.length);
        
        for(int i = 0; i <= allObject.length -1; i += 4){
            //System.out.println(allObject[i] + "plat");
            int id = Integer.parseInt(allObject[i]);
            String nombre = allObject[i+1];
            String descripcion = allObject[i+2];
            Double precio = Double.parseDouble(allObject[i+3]);
            
            plat = new Plato(id, nombre, descripcion, precio);
            platos.add(plat);
        }
        /*
        for (Plato item : platos) {
            System.out.println(item.getName());
        }*/
        
        /*obtener restaurante*/
        allObject = Arrays.copyOfRange(array, 3, array.length);
        
        for(int i = 0; i <= allObject.length -1; i += 500){
            //System.out.println(allObject[i] + "restau");
            int idrest = Integer.parseInt(allObject[i]);
            String nombre = allObject[i+1];
            String tipoComida = allObject[i+2];
            String dir = allObject[i+3];
            Double valoracion = Double.parseDouble(allObject[i+4]);
            Double pedidoMin = Double.parseDouble(allObject[i+5]);
            String horaApert = allObject[i+6];
            String horaCierre = allObject[i+7];
            //boolean open = Boolean.parseBoolean(allObject[i+8]);
            
            rest = new Restaurant(idrest, nombre, tipoComida, dir, valoracion, pedidoMin, horaApert, horaCierre, platos);
        }
        //contiene id del propietario
        //propietario.setId(Integer.parseInt(array[1]));     
        propietario = new Propietario(Integer.parseInt(cadena.split("#")[4]), rest);

        return propietario;      
    }
    public static Client getClient(String cadena){
        Client cl = new Client(Integer.parseInt(cadena.split("#")[4]), 
                Integer.parseInt(cadena.split("#")[7]), cadena.split("#")[5], cadena.split("#")[6]);
        return cl;
    }
    public static int getTotalRestaurantes(String cadena){
        return Integer.parseInt(cadena.split("#")[8]);
    }
    
    public static ArrayList<Restaurant> getRestaurantes(String cadena){
        ArrayList<Restaurant> rests = new ArrayList();        
        Restaurant restaurante = null;
        String[] array = cadena.split("CLIENTE")[1].split("#");
        String[] allRest = new String[array.length];
        
        /*obtener restaurantes*/
        allRest = Arrays.copyOfRange(array, 6, array.length);
   
        for(int i = 0; i <= allRest.length -1; i += 8){
            int idrest = Integer.parseInt(allRest[i]);
            String nombre = allRest[i+1];
            String tipoComida = allRest[i+2];
            String dir = allRest[i+3];
            Double valoracion = Double.parseDouble(allRest[i+4]);
            Double pedidoMin = Double.parseDouble(allRest[i+5]);
            String horaApert = allRest[i+6];
            String horaCierre = allRest[i+7];
            //boolean open = Boolean.parseBoolean(allRest[i+8]);
            
            restaurante = new Restaurant(idrest, nombre, tipoComida, dir, valoracion , pedidoMin, horaApert, horaCierre, null);
            rests.add(restaurante);
        }
        
        return rests;
    }

    public static String getStateRestOnline(){
        return "#CHANGE_STATE#ONLINE#" + CristEatClient.getPropietario().getRestaurante().getIdrest();
    }
    
    public static String getStateRestOffline(){     
        return "#CHANGE_STATE#OFFLINE#" + CristEatClient.getPropietario().getRestaurante().getIdrest();
    }

    
    public static Restaurant getRest(String cadena){
        Restaurant rest = new Restaurant(Integer.parseInt(cadena.split("#")[5]), cadena.split("#")[6],
                cadena.split("#")[7], cadena.split("#")[8], Double.parseDouble(cadena.split("#")[9]),
                Double.parseDouble(cadena.split("#")[10]), cadena.split("#")[11], cadena.split("#")[12],
                null);
        return rest;
    }
    
    public static String getReceivedChangeStateOffline(String idRest, String idClient){
        String msgProtocol = "#RECEIVED_CHANGE_STATE#OFFLINE#";
        return msgProtocol + idRest + "#" + idClient;
    }
    public static String getReceivedChangeStateOnline(String idRest, String idClient){
        String msgProtocol = "#RECEIVED_CHANGE_STATE#ONLINE#";
        return msgProtocol + idRest + "#" + idClient;
    }
    
    public static String getPlatos(String idRest){
        return "#GET_PLATOS#" + idRest;
    }
    
    public static ArrayList<Plato> getPlatosForViewClient(String cadena){
        Plato plat = null;
        ArrayList<Plato> platos = new ArrayList();
        String[] array = cadena.split("RESPONSE_GET_PLATOS")[1].split("#");
        String[] allObject = new String[array.length];

        /*obtener platos*/
        allObject = Arrays.copyOfRange(array, 3, array.length);
        
        for(int i = 0; i < allObject.length; i += 4){
            //System.out.println(allObject[i] + "plat");
            int id = Integer.parseInt(allObject[i]);
            String nombre = allObject[i+1];
            String descripcion = allObject[i+2];
            Double precio = Double.parseDouble(allObject[i+3]);
            
            plat = new Plato(id, nombre, descripcion, precio);
            platos.add(plat);
        }
        return platos;
    }
    
    public static String getDetailsPlatos(String idPlato){
        return "#GET_DETAIL#" + idPlato;
    }
    
    public static double getTotalBytes(String cadena){
        return Double.parseDouble(cadena.split("#")[4]);
    }
    
    public static String getIdPlatoFromResponde(String cadena){
        return cadena.split("#")[3];
    }
    
    public static int getSizePackg(String cadena){
        return Integer.parseInt(cadena.split("#")[5]);
    }
    
    public static String getTransmissionBytes(String cadena){
        return cadena.split("#")[6];
    }
    
    public static String getFotoReceived(String idPlato){
        return "#FOTO_RECEIVED#" + idPlato;
    }
    public static String getVideoReceived(String idPlato){
        return "#VIDEO_RECEIVED#" + idPlato;
    }
    
    public static String insertPlato(String idRest, String nombrePlat, String descrip, float precio){
        return "#SUBIR_INFO_PLATO#" + idRest + "#" + nombrePlat + "#" + descrip + "#" + Float.toString(precio);
    }
    
    public static String getIdNewPlat(String cadena){
        return cadena.split("#")[4];
    }
    
    public static String sendMultimediaFotoStart(String idPlato){
        return "#SUBIR_FOTO_PLATO#STARTING_MULTIMEDIA_TRANSMISSION#" + idPlato;
    }
    
    public static String sendMultimediaFotoEnd(String idPlato){
        return "#SUBIR_FOTO_PLATO#ENDING_MULTIMEDIA_TRANSMISSION#" + idPlato;
    }
        public static String sendMultimediaVideoStart(String idPlato){
        return "#SUBIR_VIDEO_PLATO#STARTING_MULTIMEDIA_TRANSMISSION#" + idPlato;
    }
    
    public static String sendMultimediaVideoEnd(String idPlato){
        return "#SUBIR_VIDEO_PLATO#ENDING_MULTIMEDIA_TRANSMISSION#" + idPlato;
    }
    
    public static void sendFoto(String idPlato, String pathImage){
        CristEatClient cecl = new CristEatClient();
        File image = new File(pathImage);
        byte[] buffer = new byte[512];
        double totalBytes = image.length();
        
        cecl.postMessageToServer(Utils.sendMultimediaFotoStart(idPlato));               

        try {                        
            FileInputStream fisImage = new FileInputStream(image);

            while (fisImage.read(buffer) > 0) {
                cecl.postMessageToServer("#SUBIR_FOTO_PLATO#" + idPlato + "#" 
                        + Double.toString(totalBytes) + "#" + "512" + "#" 
                        + Base64.getEncoder().encodeToString(buffer));
            }
            fisImage.close();
            cecl.postMessageToServer(Utils.sendMultimediaFotoEnd(idPlato));
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    public static void sendVideo(String idPlato, String pathVid){
        CristEatClient cecl = new CristEatClient();
        File video = new File(pathVid);
        byte[] buffer = new byte[512];
        double totalBytes = video.length();
        
        cecl.postMessageToServer(Utils.sendMultimediaVideoStart(idPlato));

        try {                        
            FileInputStream fisVid = new FileInputStream(video);

            while (fisVid.read(buffer) > 0) {
                cecl.postMessageToServer("#SUBIR_VIDEO_PLATO#" + idPlato + "#" 
                        + Double.toString(totalBytes) + "#" + "512" + "#" 
                        + Base64.getEncoder().encodeToString(buffer));
            }
            fisVid.close();
            cecl.postMessageToServer(Utils.sendMultimediaVideoEnd(idPlato));
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }       
    }
}
