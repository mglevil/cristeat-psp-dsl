/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import echoclient.CristEatClient;
import Models.Restaurant;
import echoclient.Utils;
import java.awt.Desktop;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;

/**
 *
 * @author mglevil
 */
public class View_client extends javax.swing.JFrame {
    private static DefaultListModel listModel;
    private static DefaultListModel listModelPlatos;
    private static DefaultListModel listModelDetails;
    private String idRest;
    private String idPlato;
    private String nomPlato;
    private static CristEatClient cecl;
    private final ImageIcon noImgIcon = new ImageIcon(getClass().getResource("/noimage.png"));
    private final ImageIcon noVidIcon = new ImageIcon(getClass().getResource("/novideo.png"));
    private File pathImg;
    private File pathVid;

    /**
     * Creates new form View_client
     */
    public View_client(String vacio){
        vacio = null;
    }
    public View_client() {        
        initComponents();
        jButtonImage.setIcon(noImgIcon);
        jButtonVid.setIcon(noVidIcon);
        
        listModel = new DefaultListModel();
        cecl = new CristEatClient();
        //jListRestaurantes.setBounds(10, 10, 10, 10); intentar cambiar tamaños de filas
        setLocationRelativeTo(null);
        addRests();
    }
    
 

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jButtonAdd = new javax.swing.JButton();
        jButtonDel = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListRestaurantes = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListPlatos = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListDetails = new javax.swing.JList<>();
        jButtonVid = new javax.swing.JButton();
        jButtonImage = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CristEAT - Cliente");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cristeat.png"))); // NOI18N

        jButtonAdd.setText("Añadir al carrito");

        jButtonDel.setText("Quitar del carrito");

        jListRestaurantes.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListRestaurantesValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListRestaurantes);

        jListPlatos.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListPlatosValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jListPlatos);

        jListDetails.setEnabled(false);
        jScrollPane3.setViewportView(jListDetails);

        jButtonVid.setBackground(java.awt.Color.white);
        jButtonVid.setEnabled(false);
        jButtonVid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVidActionPerformed(evt);
            }
        });

        jButtonImage.setBackground(java.awt.Color.white);
        jButtonImage.setEnabled(false);
        jButtonImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonImageActionPerformed(evt);
            }
        });

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(jList1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonDel, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonImage, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonVid, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane4))
                .addGap(18, 18, 18))
            .addGroup(layout.createSequentialGroup()
                .addGap(281, 281, 281)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonDel))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButtonVid, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButtonImage, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jListRestaurantesValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListRestaurantesValueChanged
        // TODO add your handling code here:       
        if (!evt.getValueIsAdjusting()){
            try {
                if(jListRestaurantes.getSelectedIndex() >= 0){
                    jButtonImage.setIcon(noImgIcon);
                    jButtonVid.setIcon(noVidIcon);

                    if(listModelDetails != null)listModelDetails.removeAllElements();//borrar detalles
                    if(jButtonImage.isEnabled() || jButtonVid.isEnabled()){
                        jButtonImage.setEnabled(false);
                        jButtonVid.setEnabled(false);
                    }
                    int i = jListRestaurantes.getSelectedIndex();
                    this.idRest = listModel.get(i).toString().split(" | ")[0];

                    cecl.postMessageToServer(Utils.getPlatos(this.idRest));

                    while(CristEatClient.havePlats == null)Thread.sleep(400);
                    CristEatClient.havePlats = null;
                    
                    addPlatos();
                }

            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }//GEN-LAST:event_jListRestaurantesValueChanged

    private void jListPlatosValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListPlatosValueChanged
        // TODO add your handling code here:
       /* Thread worker = new Thread( new Runnable() {    
            private File pathVid;
            private File pathImg;
            private String idPlato;
            private String nomPlato;
            public void run() {*/
        jButtonImage.setIcon(noImgIcon);
        jButtonVid.setIcon(noVidIcon);
        if (!evt.getValueIsAdjusting()){
            if(jListPlatos.getSelectedIndex() >= 0){
                try {                    
                    int i = jListPlatos.getSelectedIndex();
                    this.idPlato = Integer.toString(CristEatClient.platos.get(i).getId());
                    this.nomPlato = CristEatClient.platos.get(i).getName();
                    
                    CristEatClient.idPlato = this.idPlato;
                    cecl.postMessageToServer(Utils.getDetailsPlatos(this.idPlato));
                    
                    
                    while(CristEatClient.complete < 1) Thread.sleep(200);
                    CristEatClient.complete = 0; 
                    //enviar confirmacion
                    cecl.postMessageToServer(Utils.getFotoReceived(idPlato));
                    this.pathImg = new File(CristEatClient.pathImg);
                    addImg(idPlato, nomPlato);
                    
                    //while(CristEatClient.complete < 1) Thread.sleep(200);
                                    
                    cecl.postMessageToServer(Utils.getVideoReceived(idPlato));
                    this.pathVid = new File(CristEatClient.pathVid);               
                    addVid();
                } catch (InterruptedException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        //}
           }

        //});
        //worker.start();  
    }//GEN-LAST:event_jListPlatosValueChanged

    private void jButtonImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonImageActionPerformed
        try {
            // TODO add your handling code here:
            Desktop.getDesktop().open(this.pathImg);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }//GEN-LAST:event_jButtonImageActionPerformed

    private void jButtonVidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVidActionPerformed
        try {
            // TODO add your handling code here:
            Desktop.getDesktop().open(this.pathVid);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }//GEN-LAST:event_jButtonVidActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(View_client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(View_client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(View_client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(View_client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new View_client().setVisible(true);
            }
        });
    }
    
    private void addImg(String idPlato, String nombre){
        Image img = new ImageIcon(CristEatClient.pathImg).getImage();
        ImageIcon icon = new ImageIcon(img.getScaledInstance(186, 156, Image.SCALE_SMOOTH));
        jButtonImage.setIcon(icon);
        
        jButtonImage.setEnabled(true);
        
        //jLabel1.setIcon(img);
        listModelDetails = new DefaultListModel();
        listModelDetails.addElement(idPlato + " | " + nombre + ".png");      
        listModelDetails.addElement(idPlato + " | " + nombre + ".avi"); 
        //Asociar el modelo de lista al JList
        jListDetails.setModel(listModelDetails);       
    }
    
    private void addVid(){     
        ImageIcon iconVid = new ImageIcon(getClass().getResource("/vid-avaiable.png"));
        jButtonVid.setIcon(iconVid);     
        jButtonVid.setEnabled(true);        
    }
    
    private void addPlatos(){
        //Crear un objeto DefaultListModel
        listModelPlatos = new DefaultListModel();
        //Recorrer el contenido del ArrayList
        for(int i = 0; i < CristEatClient.platos.size(); i++) {
            listModelPlatos.addElement(CristEatClient.platos.get(i));            
        }
        //Asociar el modelo de lista al JList
        jListPlatos.setModel(listModelPlatos);
    }
    
    private void addRests(){
        //Recorrer el contenido del ArrayList
        for(int i = 0; i < CristEatClient.getRests().size(); i++) {
            listModel.addElement(CristEatClient.getRests().get(i));
        }
        //Asociar el modelo de lista al JList
        jListRestaurantes.setModel(listModel);
    }
    
    public void addRestBroadcast(Restaurant rest){
        listModel.addElement(rest);      
        //enviar mensaje
        cecl.postMessageToServer(Utils.getReceivedChangeStateOnline(Integer.toString(rest.getIdrest()), Integer.toString(CristEatClient.idClient)));
    }
    
    public void deleteRestBroadCast(String idRest){ 
        //String idRest = CristEatClient.idRestForDelete;
        for(int i = 0; i < listModel.size(); i++){
           if(listModel.get(i).toString().split(" | ")[0].equals(idRest)){    
               try{
                    if((listModelPlatos != null && jListRestaurantes.getSelectedIndex() == i) 
                       || (jListRestaurantes.isSelectionEmpty() && listModelPlatos != null))listModelPlatos.removeAllElements();
               
               jListRestaurantes.clearSelection();
               listModel.remove(i);
               }catch(Exception ex){
                
               }
           }
        }
        //enviar mensaje
        cecl.postMessageToServer(Utils.getReceivedChangeStateOffline(idRest, Integer.toString(CristEatClient.idClient)));
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDel;
    private javax.swing.JButton jButtonImage;
    private javax.swing.JButton jButtonVid;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jListDetails;
    private static javax.swing.JList<String> jListPlatos;
    private static javax.swing.JList<String> jListRestaurantes;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    // End of variables declaration//GEN-END:variables
}
