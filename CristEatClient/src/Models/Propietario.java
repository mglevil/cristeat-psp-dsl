/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author mglevil
 */
public class Propietario {
    private int id;
    private Restaurant restaurante;
    
    public Propietario(int id, Restaurant rest){
        this.id = id;
        this.restaurante = rest;      
    }
    public Propietario(){
        id = 0;
        restaurante = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Restaurant getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurant restaurante) {
        this.restaurante = restaurante;
    }
    
}
