/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.ArrayList;

/**
 *
 * @author mglevil
 */
public class Restaurant{
    private int idrest;
    private String name;
    private String tipoComida;
    private String dir;
    private double valoracion;
    private double pedidoMin;
    private String horaApert;
    private String horaCierre;
    private boolean open;
    private ArrayList<Plato> platos;

    
    public Restaurant(int idrest, String name, String tipoComida, String dir, 
                        double valoracion, double pedidoMin, String horaApert,
                            String horaCierre, ArrayList<Plato> platos){
        this.idrest = idrest;
        this.name = name;
        this.tipoComida = tipoComida;
        this.dir = dir;
        this.valoracion = valoracion;
        this.pedidoMin = pedidoMin;
        this.horaApert = horaApert;
        this.horaCierre = horaCierre;
        this.platos = platos;
    }

    public int getIdrest() {
        return idrest;
    }

    public ArrayList<Plato> getPlatos() {
        return platos;
    }

    public void setPlatos(ArrayList<Plato> platos) {
        this.platos = platos;
    }

    public void setIdrest(int idrest) {
        this.idrest = idrest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTipoComida() {
        return tipoComida;
    }

    public void setTipoComida(String tipoComida) {
        this.tipoComida = tipoComida;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public double getValoracion() {
        return valoracion;
    }

    public void setValoracion(double valoracion) {
        this.valoracion = valoracion;
    }

    public double getPedidoMin() {
        return pedidoMin;
    }

    public void setPedidoMin(double pedidoMin) {
        this.pedidoMin = pedidoMin;
    }

    public String getHoraApert() {
        return horaApert;
    }

    public void setHoraApert(String horaApert) {
        this.horaApert = horaApert;
    }

    public String getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(String horaCierre) {
        this.horaCierre = horaCierre;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }
    

    @Override
    public String toString() {
        return idrest + " | " + name + " | " + tipoComida + " | " + dir +  " | " + valoracion + " | " +  pedidoMin + " | " + horaApert + " | " + horaCierre;
    }
    
    
}
