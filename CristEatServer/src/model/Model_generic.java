/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import views.ServerGUI;

/**
 *
 * @author mglevil
 */
public abstract class Model_generic {
    public static String url = "";
    private final String login = "root";
    private final String password = "root";
    private String login_user;
    private String passwd;
    Connection conn;
    boolean status;
    
    /*
    public Model_generic(String url){
        this.url = url;
    }*/

    public Model_generic(String login_us, String password_us){
        this.login_user = login_us;
        this.passwd = password_us;

        try{
            conn = DriverManager.getConnection(Model_generic.url, login, password);
            this.status = true;
        }
        catch(SQLException ex){
            this.status = false;
            ServerGUI.jTextAreaLog.append(ex.getMessage() +  "Compruebe la Direccion a la BD\n");
        }catch(Exception e){
            this.status = false;
            ServerGUI.jTextAreaLog.append(e.getMessage() + "\n");
        }      
    }
    
    public boolean getStatus(){
        return status;
    }

    public String getUser(){
        return this.login_user;
    }
    public String getPasswd(){
        return this.passwd;
    }

    public String getUrl() {
        return url;
    }
  
}
