/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author mglevil
 */
public class Plato {
    private int id;
    private String name;
    private String descripcion;
    private double precio;
    private String urlImg;
    private String urlVideo;
    private int restaurant;
    
    public Plato(int id, String name, String descripcion, double precio, String img, String video, int rest){
        this.id = id;
        this.name = name;
        this.descripcion = descripcion;
        this.precio = precio;
        this.urlImg = img;
        this.urlVideo = video;
        this.restaurant = rest;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(int restaurant) {
        this.restaurant = restaurant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    @Override
    public String toString() {
        return id + "#"  + name + "#" + descripcion + "#" + precio + "#";
    }
    
    public String toStringForClient(){
        return id + "#"  + name + "#" + descripcion + "#" + precio + "#";
    }
    
}
