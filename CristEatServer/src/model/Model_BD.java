/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import server.Utils;
import views.ServerGUI;
/**
 *
 * @author user
 */
public class Model_BD extends Model_generic {
    private Statement stmt = null;  
    private boolean isPropietario = false;
    private Propietario propietario;
    private int id_user;
    private Client cliente;
    private ArrayList<Restaurant> restaurantesSinPlatos;
    private ArrayList<Restaurant> restaurantesConPlatos;
    Restaurant restClBroadCast;
    private String pathImage;
    private String pathVideo;
    
    public Model_BD(String login, String password) throws SQLException{
        super(login, password);      
        this.status = checkUser();       
    }

    public ArrayList<Restaurant> getRestaurantesConPlatos() {
        return restaurantesConPlatos;
    }

    public void setRestaurantesConPlatos(ArrayList<Restaurant> restaurantesConPlatos) {
        this.restaurantesConPlatos = restaurantesConPlatos;
    }
    

    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }

    public Client getCliente() {
        return cliente;
    }

    public void setCliente(Client cliente) {
        this.cliente = cliente;
    }

    public ArrayList<Restaurant> getRestaurantesSinPlatos() {
        return restaurantesSinPlatos;
    }

    public void setRestaurantesSinPlatos(ArrayList<Restaurant> restaurantes) {
        this.restaurantesSinPlatos = restaurantes;
    }


    public String setQuery(String pquery) {
           
        try {
            Statement stmtUp = conn.createStatement();
            stmtUp.executeUpdate(pquery);
        } catch (SQLException ex) {
            //this.status = false;
            ServerGUI.jTextAreaLog.append("No se pudo actualizar estado - " + ex.getMessage() + "\n");
        }
        return "";
    }
   
    
    private boolean checkUser() throws SQLException{
        try {                    
            String query = Utils.getQueryUserLogin(this.getUser(), this.getPasswd());
            
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            boolean existeUser = rs.next();
            
            if(existeUser){
                id_user = rs.getInt("id");//id usuario
                String query2 = Utils.getAllInfoPropietario(Integer.toString(id_user));
                ResultSet rs2 = stmt.executeQuery(query2);
               
                if(rs2.next()){/*Tipo propietario*/
                    
                    isPropietario = true;
                    String query3 = Utils.getRestaurantePropietario(rs2.getInt("idRestaurante"));                    
                    getTipoPropietario(query3);
                    
                    getAllRestaurantsConPlatos(Utils.getAllRestaurante());
                }else{/*Tipo cliente*/
                    isPropietario = false;
                    
                    String query4 = Utils.getClientInfo(this.id_user);
                    getClientInfo(query4);
                    
                    String query5 = Utils.getAllRestaurante();
                    getAllRestaurantsOpenSinPlato(query5);
                }
            }
            return existeUser;
            
        }catch (SQLException e ) {
            this.status = false;
            ServerGUI.jTextAreaLog.append(e.getMessage() + "\n");
        }finally {
            if (stmt != null) stmt.close();
        }
        
        return true;
    }
    
    public ArrayList<Plato> getAllPlatos(int idrest) throws SQLException{
        //consulta platos
        ArrayList<Plato> platos = new ArrayList();
        Statement stmt = conn.createStatement();
        String queryAllInfoPlato = "select * from plato where idRestaurante = " + idrest;
        ResultSet rs4 = stmt.executeQuery(queryAllInfoPlato);
        

        while(rs4.next()){
            int idplat = rs4.getInt("id");
            String nombre = rs4.getString("nombre");
            String descrip = rs4.getString("descripcion");
            double precio = rs4.getDouble("precio");
            String rutaFoto = rs4.getString("foto");
            String rutaVideo = rs4.getString("video");
            int idRest = rs4.getInt("idRestaurante");

            Plato plato = new Plato(idplat, nombre, descrip, precio, rutaFoto, rutaVideo, idRest);
            platos.add(plato);
        }          
        return platos;
    }
    public boolean getIsPropietario(){
        return this.isPropietario;
    }

    private void getTipoPropietario(String query3) throws SQLException {
        try {
            Statement stmt3 = conn.createStatement();
            ResultSet rs3 = stmt3.executeQuery(query3);
            
            if(rs3.next()){
                int idrest = rs3.getInt("id");
                String name = rs3.getString("nombre");
                String tipoComida = rs3.getString("tipoComida");
                String dir = rs3.getString("direccion");
                double valoracion = rs3.getDouble("valoracion");
                double pedidoMin = rs3.getDouble("pedidoMinimo");
                String horaApert = rs3.getString("horaApertura");
                String horaCierre = rs3.getString("horaCierre");
                boolean open = rs3.getBoolean("abierto");
                
                ArrayList<Plato> plato = getAllPlatos(idrest);
                Restaurant res = new Restaurant(idrest, name, tipoComida, dir, valoracion, pedidoMin, horaApert, horaCierre, open, plato);
                this.propietario = new Propietario(idrest, res);
                
            }
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");          
        }
    }

    private void getClientInfo(String query4) throws SQLException {
        try {
            Statement stmt4 = conn.createStatement();
            ResultSet rs4 = stmt4.executeQuery(query4);
            
            if(rs4.next()){
                int idClient = rs4.getInt("id");
                int numTarjetaCredito = rs4.getInt("numTarjetaCredito");                                                                      
                this.cliente = new Client(idClient, numTarjetaCredito, this.getUser(), this.getPasswd());
            }
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
        }
    
    }

    private void getAllRestaurantsOpenSinPlato(String query5) throws SQLException {
        try {
            this.restaurantesSinPlatos = new ArrayList();
            Statement stmt5 = conn.createStatement();
            ResultSet rs5 = stmt5.executeQuery(query5);
            
            while(rs5.next()){
                int idrest = rs5.getInt("id");
                String name = rs5.getString("nombre");
                String tipoComida = rs5.getString("tipoComida");
                String dir = rs5.getString("direccion");
                double valoracion = rs5.getDouble("valoracion");
                double pedidoMin = rs5.getDouble("pedidoMinimo");
                String horaApert = rs5.getString("horaApertura");
                String horaCierre = rs5.getString("horaCierre");
                boolean open = rs5.getBoolean("abierto");
                                         
                Restaurant rest = new Restaurant(idrest, name, tipoComida, dir, valoracion, pedidoMin, horaApert, horaCierre, open, null);
                this.restaurantesSinPlatos.add(rest);
            }
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
        }
    }
    
    private void getAllRestaurantsConPlatos(String query6) throws SQLException {
        try {
            this.restaurantesConPlatos = new ArrayList();
            Statement stmt6 = conn.createStatement();
            ResultSet rs6 = stmt6.executeQuery(query6);
            
            while(rs6.next()){
                int idrest = rs6.getInt("id");
                String name = rs6.getString("nombre");
                String tipoComida = rs6.getString("tipoComida");
                String dir = rs6.getString("direccion");
                double valoracion = rs6.getDouble("valoracion");
                double pedidoMin = rs6.getDouble("pedidoMinimo");
                String horaApert = rs6.getString("horaApertura");
                String horaCierre = rs6.getString("horaCierre");
                boolean open = rs6.getBoolean("abierto");
                
                ArrayList<Plato> plato = getAllPlatos(idrest);                           
                Restaurant rest = new Restaurant(idrest, name, tipoComida, dir, valoracion, pedidoMin, horaApert, horaCierre, open, plato);
                this.restaurantesConPlatos.add(rest);
            }
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
        }
    }
    
    public Restaurant getQueryRestBroadCast(String pquery) {
        
        try {       
            Statement stmtGet = conn.createStatement();
            ResultSet rs7 = stmtGet.executeQuery(pquery);
            while(rs7.next()){
                int idrest = rs7.getInt("id");
                String name = rs7.getString("nombre");
                String tipoComida = rs7.getString("tipoComida");
                String dir = rs7.getString("direccion");
                double valoracion = rs7.getDouble("valoracion");
                double pedidoMin = rs7.getDouble("pedidoMinimo");
                String horaApert = rs7.getString("horaApertura");
                String horaCierre = rs7.getString("horaCierre");
                boolean open = rs7.getBoolean("abierto");
                                         
                this.restClBroadCast = new Restaurant(idrest, name, tipoComida, dir, valoracion, pedidoMin, horaApert, horaCierre, open, null);               
            }           
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append("No se pudo obtener query - " + ex.getMessage() + "\n");
        }
        return restClBroadCast;
    }
    
    public String getPathImage(String query){        
        try {
            
            Statement stmtGet = conn.createStatement();
            ResultSet rs8 = stmtGet.executeQuery(query);
            
            while(rs8.next()){
                this.pathImage = rs8.getString("foto");
            }
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append("No se pudo obtener foto - " + ex.getMessage() + "\n");
        }
        return this.pathImage;
    }
    
    public String getPathVideo(String query){        
        try {
            
            Statement stmtGet = conn.createStatement();
            ResultSet rs9 = stmtGet.executeQuery(query);
            
            while(rs9.next()){
                this.pathVideo = rs9.getString("video");
            }
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append("No se pudo obtener video - " + ex.getMessage() + "\n");
        }
        return this.pathVideo;
    }
    
    public String setInsertPlat(String query){
        try {
            Statement stmtInsert = conn.createStatement();
            stmtInsert.executeUpdate(query);           
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append("No se pudo insertar plato - " + ex.getMessage() + "\n");
        }
        return "";
    }
    
    public String getIdNewPlato(String query){
        int idNew = 0;
        try {            
            Statement stmtGetId = conn.createStatement();
            ResultSet rs10 = stmtGetId.executeQuery(query); 
            while(rs10.next()){
                idNew = rs10.getInt("id");
            }
        } catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append("No se pudo obtener id del plato nuevo - " + ex.getMessage() + "\n");
        }
        return Integer.toString(idNew);
    }
}
