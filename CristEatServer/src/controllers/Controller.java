/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import model.Model_BD;
import java.sql.SQLException;
import model.Restaurant;

/**
 *
 * @author user
 */
public class Controller {
    //private String url;
    private String login;
    private String password;
    private Model_BD model;
    
    public Controller(String login, String password){
        this.login = login;
        this.password = password;
    }

    public boolean setConnectDB() throws SQLException{
        Model_BD model = new Model_BD(login, password);  
        //referenciar variable de clase
        this.model = model;
        return model.getStatus();
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
    
    public void getStatus(){
        model.getStatus();
    }
 
    public String setQueryUpdate(String pquery){
        return model.setQuery(pquery);       
    }
    
    public Restaurant getQueryBroadCast(String query){
        return model.getQueryRestBroadCast(query);
    }
    
    public String getPathImage(String query){
        return model.getPathImage(query);
    }
    
    public String getPathVideo(String query){
        return model.getPathVideo(query);
    }
    
    public String setInsertPlat(String query){
        return model.setInsertPlat(query);
    }
    
    public String getIdNewPlato(String query){
        return model.getIdNewPlato(query);
    }
    
    public Model_BD getUser(){
        return this.model;
    }

}
