/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import views.ServerGUI;

/**
 *
 * @author mglevil
 */
public class MultiServer {
    int portNumber;
    boolean listening;
    static ArrayList<MultiServerThread> users;
    static ServerSocket serverSocket;
    
    public MultiServer(int port) throws IOException{
        this.portNumber = port;
        this.listening = true;
        this.users = new ArrayList();
        
        try{
            this.serverSocket = new ServerSocket(portNumber);
            while (listening) {
                acceptConex();
                deleteConex();
            }
        }catch(IOException ex){
            ServerGUI.jTextAreaLog.append("Error: No se pudo conectar" + "\n");
        }
    }
      
    public synchronized void acceptConex(){
        
        try{
            users.add(new MultiServerThread(serverSocket.accept(), serverSocket.getInetAddress().toString()));
        }catch(IOException ex){
            ServerGUI.jTextAreaLog.append("Error: No se pudo conectar" + ex.getMessage() + "\n");
        }
        
        users.get(users.size()-1).start();
        ServerGUI.jTextAreaLog.append("Tamaño array usuarios: " + (users.size()-1) + "\n");
    }

    
    public synchronized void deleteConex(){
        for(int i = 0; i < users.size(); i++){
            if(!users.get(i).isAlive() || users.get(i).isDaemon()){
                try {
                    users.get(i).join();
                    users.remove(i);
                    ServerGUI.jTextAreaLog.append("Usuario en hebra: " + i + " delete\n");
                } catch (InterruptedException ex) {
                    ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
                }
                
                //borar hebras propietarias para multidifusion
                for (int j = 0; j < hebraPropietario.hp.size(); j++) {
                    if(Integer.parseInt(hebraPropietario.hp.get(j).split("#")[2]) == i){
                        hebraPropietario.hp.remove(j);
                    }
                }
                //borar hebras clientes para multidifusion
                for (int k = 0; k < hebraCliente.hc.size(); k++) {
                    if(hebraCliente.hc.get(k) == i){
                        hebraCliente.hc.remove(k);
                    }
                }
            }
        }
    }
    
    public static synchronized void postBroadcast(String cadena){        
        for(int i = 0; i < users.size(); i++){
            users.get(i).broadCast.println(cadena);
            ServerGUI.jTextAreaLog.append("Se envio al cliente " + "[" + i + "]: " + cadena + "\n");
        }
    }
    
    public static synchronized void postConfirmedState(String message, int hebra){        
        users.get(hebra).broadCast.println(message);
        ServerGUI.jTextAreaLog.append("Se envio al propietario " + "[" + hebra + "]: " + message + "\n");

    }
    
    public void stopServer(){
        this.listening = false;
        try {
            serverSocket.close();
            ServerGUI.jTextAreaLog.append("Stopped....\n");
        } catch (IOException ex) {
            ServerGUI.jTextAreaLog.append("Problema al desconectar: " + ex.getMessage() + "\n");
            
        }
}
    
}
