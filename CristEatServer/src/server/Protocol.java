/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import controllers.Controller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Base64;
import views.ServerGUI;

/**
 *
 * @author mglevil
 */
public class Protocol {
    Controller controller; 
    public static String login;
    private static int received_change;
    private int hebra;
    private int hebraCl;
    private hebraCliente hc;
    private final int sizePack = 512;
    private PrintWriter out;
    private String idRest;
    private String idNewplat;
    private FileOutputStream fosImg;
    private FileOutputStream fosVid;
    private byte[] bufferIn;
    
    public Protocol(Socket socket){
        try {
            this.out = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException ex) {
           ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
        }
    }
 
    public String processInput(String theInput) throws SQLException {      
        String theOutput = null;
        ServerGUI.jTextAreaLog.append("El cliente envio: " + theInput + "\n");
        
        if(theInput.substring(theInput.length()-1).equals("#")){
            ServerGUI.jTextAreaLog.append("#######INVALID_PROTOCOL######\nSe intentara corregir\n");
        }       
        /*Autenticacion*/        
        if(theInput.contains("#LOGIN#") && !theInput.contains("#CHANGE_STATE#") 
                && !theInput.contains("#RECEIVED_CHANGE_STATE#") && !theInput.contains("#GET_PLATOS#")
                && !theInput.contains("#GET_DETAIL#")){
            
            login = Utils.getUsers(theInput);
            String password = Utils.getPasswd(theInput);

            controller = new Controller(login, password);
            boolean state = controller.setConnectDB();
            
            if(state){
                controller.getUser().getIsPropietario();
                if(controller.getUser().getIsPropietario()){      
                    theOutput = Utils.getDataFromPropietario(controller.getUser().getPropietario());                   
                    ServerGUI.jTextAreaLog.append("Se envio al propietario: " + theOutput + "\n");
                    
                    hebra = MultiServer.users.size()-1;//obtener hebra propietario
                    hebraPropietario.hp.add(controller.getUser().getPropietario().toString() + Integer.toString(hebra));
                }else{
                    theOutput = Utils.getDataFromClient(controller.getUser().getCliente(), controller.getUser().getRestaurantesSinPlatos());                    
                    ServerGUI.jTextAreaLog.append("Se envio al cliente: " + theOutput + "\n");

                    hebraCl = MultiServer.users.size()-1;
                    hebraCliente.hc.add(hebraCl);
                }              
            }else{
                theOutput = "PROTOCOLCRISTEAT1.0#SERVER#ERROR#BAD_LOGIN";
                ServerGUI.jTextAreaLog.append("Se envio al cliente: " + theOutput + "\n");
            } 
    
        }else{           
            /*Enviar platos cliente*/
            if(theInput.contains("#GET_PLATOS#")){
                int idRestForPlatos = Utils.getIdRestForPlatos(theInput);
                theOutput = Utils.getPlatosClient(idRestForPlatos, controller.getUser().getAllPlatos(idRestForPlatos));
                ServerGUI.jTextAreaLog.append("Se envio al cliente: " + theOutput + "\n");
            }
            
            /*Cambio de estado y notificacion*/
            if(theInput.contains("#CHANGE_STATE#")){
                String idRest = Utils.getIdRest(theInput);                
                
                if(theInput.contains("ONLINE")){
                    controller.setQueryUpdate(Utils.setStateOnline(idRest));
                    /*BROADCAST para clientes*/                       
                    MultiServer.postBroadcast(Utils.getOnlineRest(controller.getQueryBroadCast(Utils.getRestForBroadCast(idRest))));                  
                    
                }else{
                    controller.setQueryUpdate(Utils.setStateOffline(idRest));
                    /*BROADCAST para clietnes*/        
                    MultiServer.postBroadcast(Utils.getOfflineRest(idRest)); 
                }
            }else if(theInput.contains("#RECEIVED_CHANGE_STATE#")){
                hc = new hebraCliente(theInput.split("#")[4]);
                received_change += 1;

                if(hebraCliente.hc.size() == received_change){
                    
                    for(int i = 0; i < hebraPropietario.hp.size(); i++){
                        if(hebraPropietario.hp.get(i).split("#")[1].equals(hc.getIdRest())){
                            MultiServer.postConfirmedState(Utils.getMessageConfirmed(), Integer.parseInt(hebraPropietario.hp.get(i).split("#")[2]));                                       
                            received_change = 0;
                        }
                    }
                    received_change = 0;
                }
            }            
            /*GET_DETAILS (FOTO, VIDEO)*/
            if(theInput.contains("#GET_DETAIL#")){              
                String idPlato = Utils.getIdPlatoForDetail(theInput);
                out.println(Utils.sendStartMultimedia(idPlato));
                ServerGUI.jTextAreaLog.append("Se envio al cliente: " + Utils.sendStartMultimedia(idPlato) + "\n");

                String pathImage = controller.getPathImage(Utils.getPathImage(idPlato).toLowerCase());
                String pathVideo = controller.getPathVideo(Utils.getPathVideo(idPlato).toLowerCase());
                
                sendFiles(idPlato, pathImage, pathVideo); 
                
                theOutput = Utils.sendEndMultimedia(idPlato);
                ServerGUI.jTextAreaLog.append("Se envio al cliente: " + theOutput + "\n");
            }
            //INSERT NEW PLATO
            if(theInput.contains("#SUBIR_INFO_PLATO#")){
                controller.setInsertPlat(Utils.setInsertPlat(theInput));//insert info basica
                this.idNewplat = controller.getIdNewPlato(Utils.getIdNewPlato(theInput));
                this.idRest = Utils.getIdRestForNewPlato(theInput);
                //update paths
                controller.setQueryUpdate(Utils.updatePathImg(theInput, idNewplat));
                controller.setQueryUpdate(Utils.updatePathVid(theInput, idNewplat));
                //create dir
                File dir = new File("data/restaurantes/" + idRest + "/" + idNewplat + "/");
                dir.mkdirs();
                
                theOutput = Utils.getConfirmedInfoNewPlat(theInput, idNewplat);
                ServerGUI.jTextAreaLog.append("Se envio al propietario: " + theOutput + "\n");
            }
            //IMAGE
            if(theInput.contains("#SUBIR_FOTO_PLATO#STARTING_MULTIMEDIA_TRANSMISSION#")){
                this.bufferIn = new byte[sizePack];              
                File img = new File("data/restaurantes/" + idRest + "/" + idNewplat + "/" + idNewplat + ".png");
                
                if (!img.exists()) {
                    try {
                        img.createNewFile();
                        this.fosImg = new FileOutputStream(img);                                         
                    } catch (IOException ex) {
                        ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
                    }
                }
            }
            
            if(theInput.contains("#SUBIR_FOTO_PLATO#") 
                    && !theInput.contains("#STARTING_MULTIMEDIA_TRANSMISSION#")
                    && !theInput.contains("#ENDING_MULTIMEDIA_TRANSMISSION#")){
                try {          
                    this.bufferIn = Base64.getDecoder().decode(Utils.getTransmissionBytes(theInput).getBytes());
                    if(this.bufferIn != null)this.fosImg.write(this.bufferIn);                                  
                } catch (IOException ex) {
                    ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
                }
            }
            
            if(theInput.contains("#SUBIR_FOTO_PLATO#ENDING_MULTIMEDIA_TRANSMISSION#")){
                try {
                    fosImg.close();
                    theOutput = Utils.sendReceivedImg(theInput);
                    ServerGUI.jTextAreaLog.append("Se envio al propietario: " + theOutput + "\n");
                } catch (IOException ex) {
                    ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
                }
            }
            
            //VIDEO
            if(theInput.contains("#SUBIR_VIDEO_PLATO#STARTING_MULTIMEDIA_TRANSMISSION#")){
                this.bufferIn = new byte[sizePack];
                File vid = new File("data/restaurantes/" + idRest + "/" + idNewplat + "/" + idNewplat + ".avi");
                
                if (!vid.exists()) {
                    try {
                        vid.createNewFile();
                        this.fosVid = new FileOutputStream(vid);                                         
                    } catch (IOException ex) {
                        ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
                    }
                }
            }
            
            if(theInput.contains("#SUBIR_VIDEO_PLATO#") 
                    && !theInput.contains("#STARTING_MULTIMEDIA_TRANSMISSION#")
                    && !theInput.contains("#ENDING_MULTIMEDIA_TRANSMISSION#")){
                try {          
                    this.bufferIn = Base64.getDecoder().decode(Utils.getTransmissionBytes(theInput).getBytes());
                    if(this.bufferIn != null)this.fosVid.write(this.bufferIn);                                  
                } catch (IOException ex) {
                    ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
                }
            }
            
            if(theInput.contains("#SUBIR_VIDEO_PLATO#ENDING_MULTIMEDIA_TRANSMISSION#")){
                try {
                    fosVid.close();
                    theOutput = Utils.sendReceivedVid(theInput);
                    ServerGUI.jTextAreaLog.append("Se envio al propietario: " + theOutput + "\n");
                } catch (IOException ex) {
                    ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
                }
            }
            
        }   
        return theOutput;
    }
    
    private void sendFiles(String idPlato, String pathImage, String pathVideo){
        File image = new File(pathImage);
        File video = new File(pathVideo);
        /*IMAGE*/
        if(image.exists()){
            byte[] buffer = new byte[sizePack];
            try {                        
                FileInputStream fisImage = new FileInputStream(image);

                while (fisImage.read(buffer) > 0) {
                    out.println(Utils.getImage(idPlato, image.length(), sizePack, buffer));
                    ServerGUI.jTextAreaLog.append("Se envio al cliente: " + Utils.getImage(idPlato, image.length(), sizePack, buffer) + "\n");
                }
                fisImage.close();
                //out.println(Utils.sendEndMultimedia(idPlato));
                //ServerGUI.jTextAreaLog.append("Se envio al cliente: " + Utils.sendEndMultimedia(idPlato) + "\n");
            } catch (IOException ex) {
                ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
            }
        }else{
            ServerGUI.jTextAreaLog.append("No existe la imagen\n");
        }
        /*VIDEO*/
        if(video.exists()){
            //out.println(Utils.sendStartMultimedia(idPlato));
            //ServerGUI.jTextAreaLog.append("Se envio al cliente: " + Utils.sendStartMultimedia(idPlato) + "\n");
            byte[] buffer = new byte[sizePack];
            try {                        
                FileInputStream fisVideo = new FileInputStream(video);
                
                while (fisVideo.read(buffer) > 0) {
                    out.println(Utils.getVideo(idPlato, video.length(), sizePack, buffer));
                    ServerGUI.jTextAreaLog.append("Se envio al cliente: " + Utils.getVideo(idPlato, video.length(), sizePack, buffer) + "\n");
                }
                fisVideo.close();        
            } catch (IOException ex) {
                ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");            
            }
        }else{
            ServerGUI.jTextAreaLog.append("No existe el video\n");
        }       
    }
}


