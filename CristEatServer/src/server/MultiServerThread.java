/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLException;
import views.ServerGUI;

/**
 *
 * @author mglevil
 */
public class MultiServerThread extends Thread{
        private Socket socket = null;
        public PrintWriter broadCast;
        private String addres;

    public MultiServerThread(Socket socket, String addres) {
        super("MultiServerThread");
        this.socket = socket;
        this.addres = addres;
    }
    
    @Override 
    public void run() {
        ServerGUI.jTextAreaLog.append("IP Cliente nuevo:" + socket.getRemoteSocketAddress().toString() + "\n");

        try{
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String inputLine, outputLine;
            Protocol proto = new Protocol(this.socket);
            broadCast = out;

            while ((inputLine = in.readLine()) != null) {
                
                outputLine = proto.processInput(inputLine);
                out.println(outputLine);
         
            }
            socket.close();
        } catch (IOException e) {
            ServerGUI.jTextAreaLog.append(e.getMessage() + "\n");          
        }   
        catch (SQLException ex) {
            ServerGUI.jTextAreaLog.append(ex.getMessage() + "\n");
        }
    }
}
