/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.util.ArrayList;

/**
 *
 * @author mglevil
 */
public class hebraPropietario {
    private int numHebra;
    private String idRest;
    public static ArrayList<String> hp = new ArrayList();
    
    public hebraPropietario(String login, int numHebra){        
        this.numHebra = numHebra;
        this.idRest = login;
    }

    public int getNumHebra() {
        return numHebra;
    }

    public void setNumHebra(int numHebra) {
        this.numHebra = numHebra;
    }

    public String getLogin() {
        return idRest;
    }

    public void setLogin(String login) {
        this.idRest = login;
    }
    
}
