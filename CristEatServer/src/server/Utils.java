/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.util.ArrayList;
import java.util.Base64;
import model.Client;
import model.Plato;
import model.Propietario;
import model.Restaurant;

/**
 *
 * @author mglevil
 */
public class Utils {
    
    /*Desguace para protocolo*/
    //devuelve el usuario en una cadena dada la estructura: PROTOCOLCRISTEAT1.0#LOGIN#LOGIN#PASS
    public static String getUsers(String cadena){        
        return cadena.split("#")[2];
    }
    public static String getPasswd(String cadena){        
        return cadena.split("#")[3];
    }
    //"PROTOCOLCRISTEAT1.0#SERVER#LOGIN_CORRECT#PROPIETARIO#[INFOPROPIA]"
    public static String getClient(String cadena){
        return cadena.split("#")[3];
    }
    public static String getDataFromPropietario(Propietario propietario){
        String msgProtocolo = "PROTOCOLCRISTEAT1.0#SERVER#LOGIN_CORRECT#PROPIETARIO";
        msgProtocolo += propietario.toString() + Protocol.login + "#" + propietario.getRestaurante().toString();  
        return msgProtocolo.substring(0, msgProtocolo.length()-1);    
    }
    public static String getDataFromClient(Client cliente, ArrayList<Restaurant> res){
        String msgProtocolo = "PROTOCOLCRISTEAT1.0#SERVER#LOGIN_CORRECT#CLIENTE";
        msgProtocolo += cliente.toString();
        msgProtocolo += res.size() + "#";
        
        for(int i = 0; i <= res.size()-1; i++){
            msgProtocolo += res.get(i).toStringSinPlatos();
        }
        
        return msgProtocolo.substring(0, msgProtocolo.length()-1);      
    }
    
    
    
    /*CONSULTAS*/
    public static String getQueryUserLogin(String login, String pass){
        return "select id from usuario u where login = '" + login + "' and password = '"+ pass + "'";
    }
    public static String getAllInfoPropietario(String id){
        return "select * from propietario where id = " + id;
    }   
    public static String getClientInfo(int id_user){
        return "select * from cliente where id = " + id_user;
    }
    public static String getAllRestaurante(){       
        return "select * from restaurante where abierto = 1";
    }
    public static String getRestaurantePropietario(int idRest){
        return "select * from restaurante where id = " + idRest;
    }  
    
    public static String getLoginPropietarioFromId(String id){
        return "select login from propietario where id = " + id;
    }
    public static String getPathImage(String idPlato){
        return "select foto from plato where id = " + idPlato;
    }
    public static String getPathVideo(String idPlato){
        return "select video from plato where id = " + idPlato;
    }

    
    /*Cambiar estados*/
    public static String getIdRest(String cadena){
        return cadena.split("#")[4];
    }
    public static String setStateOnline(String idRest){
        return "update restaurante set abierto = true where id = " + idRest;
    }
    public static String setStateOffline(String idRest){
        return "update restaurante set abierto = false where id = " + idRest;
    }
    
        
    /*BROADCAST*/
    public static String getRestForBroadCast(String idRest){
        return "select * from restaurante where id = " + idRest;
    }
    
    public static String getOnlineRest(Restaurant rest){
        String msgProtocolo = "PROTOCOLCRISTEAT1.0#SERVER#BROADCAST#CHANGE_STATE#ONLINE#";     
        return msgProtocolo + rest.toStringSinPlatos();
    }
    
    public static String getOfflineRest(String idRest){
        String msgProtocolo = "PROTOCOLCRISTEAT1.0#SERVER#BROADCAST#CHANGE_STATE#OFFLINE#";
        return msgProtocolo + idRest;
    }        
    
    public static String getLoginPropietario(String cadena){
        return cadena.split("#")[1] + "#";
    }
    
    public static String getMessageConfirmed(){
        return "PROTOCOLCRISTEAT1.0#SERVER#CONFIRMED_STATE_CHANGE";
    }

    
    
    /*Platos cliente*/
    public static int getIdRestForPlatos(String cadena){
        return Integer.parseInt(cadena.split("#")[3]);
    }
    
    public static String getPlatosClient(int idRest, ArrayList<Plato> platos){
        String msgProtocol = "PROTOCOLCRISTEAT1.0#SERVER#RESPONSE_GET_PLATOS#";
        msgProtocol += Integer.toString(idRest) + "#";
        msgProtocol += platos.size() + "#";
        
        for(int i = 0; i < platos.size(); i++ ){
            msgProtocol += platos.get(i).toStringForClient();
        }
        
        return msgProtocol.substring(0, msgProtocol.length()-1);
    }
    
    /*Detalles de platos*/
    public static String getIdPlatoForDetail(String cadena){
        return cadena.split("#")[3];
    }
    public static String sendStartMultimedia(String idPlato){
        return "PROTOCOLCRISTEAT1.0#SERVER#STARTING_MULTIMEDIA_TRANSMISSION#" + idPlato;
    }
    public static String sendEndMultimedia(String idPlato){
        return "PROTOCOLCRISTEAT1.0#SERVER#ENDING_MULTIMEDIA_TRANSMISSION#" + idPlato;
    }
    //PROTOCOLCRISTEAT1.0#SERVER#RESPONSE_MULTIMEDIA#ID_PLATO#TOTAL_BYTES_FOTO#SIZE_PACKET_FOTO#@1024BYTES_FOTO#
    public static String getImage(String idPlato, double totalBytes, int sizePack, byte[] buffer){     
        String msgProtocol = "PROTOCOLCRISTEAT1.0#SERVER#RESPONSE_MULTIMEDIA_PHOTO#";
        return msgProtocol + idPlato + "#" + Double.toString(totalBytes) + "#" 
                + Integer.toString(sizePack) + "#" + Base64.getEncoder().encodeToString(buffer);
    }
    
    public static String getVideo(String idPlato, double totalBytes, int sizePack, byte[] buffer){
        String msgProtocol = "PROTOCOLCRISTEAT1.0#SERVER#RESPONSE_MULTIMEDIA_VIDEO#";
        return msgProtocol + idPlato + "#" + Double.toString(totalBytes) + "#" 
                + Integer.toString(sizePack) + "#" + Base64.getEncoder().encodeToString(buffer);
    }

    //insert plato
    public static String setInsertPlat(String cadena){
        String idRest = cadena.split("#")[3];
        String name = cadena.split("#")[4];
        String descrip = cadena.split("#")[5];
        String precio = cadena.split("#")[6];

        return "insert into plato values( 0,'" + name + "', '" + descrip + "', " 
                + precio + ", 'data/restaurantes/" + idRest 
                + "/', 'data/restaurantes/" + idRest + "/', " + idRest +")";
    }
    
    public static String getConfirmedInfoNewPlat(String cadena, String idNewPlat){
        String idRest = cadena.split("#")[3];
        return "PROTOCOLCRISTEAT1.0#SERVER#RECEIVED_SUBIR_INFO_PLATO#" + idRest + "#" + idNewPlat;
    }
    
    public static String getIdNewPlato(String cadena){
        String idRest = cadena.split("#")[3];
        return "select id from plato where idRestaurante = " + idRest + " order by id desc limit 1";
    }
    
    public static String updatePathImg(String cadena, String idNewplat){
        String idRest = cadena.split("#")[3];
        return "update plato set foto = 'data/restaurantes/" + idRest +  "/" 
                + idNewplat + "/" + idNewplat + ".png' where id = " + idNewplat;
    }
    
    public static String updatePathVid(String cadena, String idNewplat){
        String idRest = cadena.split("#")[3];
        return "update plato set video = 'data/restaurantes/" + idRest +  "/" 
                + idNewplat + "/" + idNewplat + ".avi' where id = " + idNewplat;
    }
    
    public static String getIdRestForNewPlato(String cadena){
        return cadena.split("#")[3];
    }
    
    public static String getTransmissionBytes(String cadena){
        return cadena.split("#")[6];
    }
    
    public static String sendReceivedImg(String cadena){
        return "PROTOCOLCRISTEAT1.0#SERVER#RECEIVED_SUBIR_FOTO_PLATO#" + cadena.split("#")[4];
    }
    
    public static String sendReceivedVid(String cadena){
        return "PROTOCOLCRISTEAT1.0#SERVER#RECEIVED_SUBIR_VIDEO_PLATO#" + cadena.split("#")[4];
    }
    
}
